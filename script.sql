create database sgt
          owner = postgres
	   template = postgres
       encoding = 'UTF8'
	 tablespace = pg_default;



create table status_tarefas (
   codigo serial primary key,
   descricao varchar(100) not null
);



create table tarefas (
	codigo serial primary key,
	titulo varchar(100) not null,
	descricao text,
	status integer references status_tarefas (codigo),
	data_criacao timestamp not null,
	data_finalizacao timestamp
);



create sequence seq_usuario
increment 1
maxvalue 999999999
minvalue 1
start 1;


create table usuarios (
	codigo integer primary key,
	nome varchar(100) not null
);



create table usuarios_tarefas (
  codigo serial primary key,
	codigo_usuario integer references usuarios (codigo),
	codigo_tarefa integer references tarefas (codigo),
	data_inicio timestamp
);



create table cursos (
   codigo serial primary key,
   descricao varchar(100) not null
);



create table usuarios_cursos (
	codigo_usuario integer references usuarios (codigo),
	codigo_curso integer references cursos (codigo)
);

INSERT INTO status_tarefas (descricao) values ('Não concluída');

INSERT INTO status_tarefas (descricao) values ('Concluída');