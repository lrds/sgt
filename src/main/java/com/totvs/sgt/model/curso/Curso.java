package com.totvs.sgt.model.curso;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity(name = "cursos")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Curso {

    @Id
    @EqualsAndHashCode.Include
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long codigo;

    @EqualsAndHashCode.Include
    private String descricao;

    public Curso(Long codigo) {
        this.codigo = codigo;
    }

//    @JsonIgnoreProperties({"cursos"})
//    @ManyToMany(mappedBy = "cursos", fetch = FetchType.EAGER)
//    private Set<Usuario> usuarios = new LinkedHashSet<>();
}
