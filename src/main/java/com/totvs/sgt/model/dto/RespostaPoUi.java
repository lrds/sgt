package com.totvs.sgt.model.dto;

import com.totvs.sgt.model.statusTarefa.StatusTarefa;
import lombok.Data;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

@Data
public class RespostaPoUi {

    private Boolean hasNext = false;
    private List items = new ArrayList();

    public RespostaPoUi(Page page) {
        this.hasNext = page.hasNext();
        this.items = page.getContent();
    }

    public RespostaPoUi(List list) {
        this.hasNext = false;
        this.items = list;
    }
}
