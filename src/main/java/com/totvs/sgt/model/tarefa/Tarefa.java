package com.totvs.sgt.model.tarefa;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.totvs.sgt.model.usuarioTarefa.UsuarioTarefa;
import com.totvs.sgt.model.statusTarefa.StatusTarefa;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tarefas")
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Tarefa {

    @Id
    @EqualsAndHashCode.Include
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long codigo;

    @EqualsAndHashCode.Include
    private String titulo;

    private String descricao;

    @ManyToOne
    @JoinColumn(name = "status", referencedColumnName = "codigo")
    private StatusTarefa statusTarefa;

    @OneToMany(mappedBy = "tarefa", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnoreProperties({"tarefa"})
    private List<UsuarioTarefa> usuarioTarefas = new ArrayList<>();

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Timestamp dataCriacao;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Timestamp dataFinalizacao;

    public void setaUsuarios() {
        this.usuarioTarefas.forEach(usuarioTarefa -> usuarioTarefa.setTarefa(this));
    }

    public List<Long> pegaCdUsuarios () {
        return this.usuarioTarefas.stream().map(usuarioTarefa -> {
            return usuarioTarefa.getUsuario().getCodigo();
        }).collect(Collectors.toList());
    }

}
