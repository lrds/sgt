package com.totvs.sgt.model.tarefa.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.totvs.sgt.model.statusTarefa.StatusTarefa;
import com.totvs.sgt.model.tarefa.Tarefa;
import com.totvs.sgt.model.usuario.Usuario;
import com.totvs.sgt.model.usuarioTarefa.UsuarioTarefa;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TarefaDTO {

    private Long codigo;
    private String titulo;
    private String descricao;
    private Long cdStatusTarefa;
    private String descStatusTarefa;
    private List<Long> cdUsuarioTarefas = new ArrayList<>();
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Timestamp dataCriacao;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Timestamp dataFinalizacao;

    public TarefaDTO(Tarefa tarefa) {
        this.codigo = tarefa.getCodigo();
        this.titulo = tarefa.getTitulo();
        this.descricao = tarefa.getDescricao();
        if (tarefa.getStatusTarefa() != null) {
            this.cdStatusTarefa = tarefa.getStatusTarefa().getCodigo();
            this.descStatusTarefa = tarefa.getStatusTarefa().getDescricao();
        }
        if (tarefa.getUsuarioTarefas() != null) {
            this.cdUsuarioTarefas = tarefa.pegaCdUsuarios();
        }
        this.dataCriacao = tarefa.getDataCriacao();
        this.dataFinalizacao = tarefa.getDataFinalizacao();
    }

    public Tarefa converteParaTarefa() {
        Tarefa tarefa = new Tarefa();
        tarefa.setCodigo(this.codigo);
        tarefa.setTitulo(this.titulo);
        tarefa.setDescricao(this.descricao);
        if (this.cdStatusTarefa != null) {
            tarefa.setStatusTarefa(new StatusTarefa(this.cdStatusTarefa));
        }
        tarefa.setUsuarioTarefas(this.carregaListaUsuario(this.cdUsuarioTarefas));
        tarefa.setDataCriacao(this.dataCriacao);
        tarefa.setDataFinalizacao(this.dataFinalizacao);

        return tarefa;
    }

    private List<UsuarioTarefa> carregaListaUsuario(List<Long> cdUsuarioTarefas) {
        return cdUsuarioTarefas.stream().map(cdUsuario -> {
            return new UsuarioTarefa(new Usuario(cdUsuario));
        }).collect(Collectors.toList());
    }


}
