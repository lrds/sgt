package com.totvs.sgt.model.tarefa.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.totvs.sgt.model.tarefa.Tarefa;
import com.totvs.sgt.model.usuarioTarefa.UsuarioTarefa;
import lombok.Data;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
public class TarefaListaDTO {

    private Long codigo;
    private String titulo;
    private String descricao;
    private Long cdStatusTarefa;
    private String descStatusTarefa;
    private List<UsuarioTarefa> usuarioTarefas = new ArrayList<>();
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Timestamp dataCriacao;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Timestamp dataFinalizacao;

    public TarefaListaDTO(Tarefa tarefa) {
        this.codigo = tarefa.getCodigo();
        this.titulo = tarefa.getTitulo();
        this.descricao = tarefa.getDescricao();
        if (tarefa.getStatusTarefa() != null) {
            this.cdStatusTarefa = tarefa.getStatusTarefa().getCodigo();
            this.descStatusTarefa = tarefa.getStatusTarefa().getDescricao();
        }
        this.usuarioTarefas = tarefa.getUsuarioTarefas();
        this.dataCriacao = tarefa.getDataCriacao();
        this.dataFinalizacao = tarefa.getDataFinalizacao();
    }

}
