package com.totvs.sgt.model.usuario;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.totvs.sgt.model.curso.Curso;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = "usuarios")
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name = "seq_usuario", sequenceName = "seq_usuario", allocationSize = 1)
public class Usuario {

    @Id
    @Column(name = "codigo")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_usuario")
    private Long codigo;

    @Column(name = "nome")
    private String nome;

    @JsonIgnoreProperties({"usuarios"})
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name="usuarios_cursos",
               joinColumns = @JoinColumn(name="codigo_usuario", referencedColumnName = "codigo"),
               inverseJoinColumns = @JoinColumn(name = "codigo_curso", referencedColumnName = "codigo"))
    private Set<Curso> cursos = new LinkedHashSet();

//    @JsonIgnoreProperties({"usuario"})
//    @OneToMany(mappedBy = "usuario", cascade = CascadeType.ALL, orphanRemoval = true)
//    private Set<UsuarioTarefa> usuarioTarefas = new LinkedHashSet();

    public Usuario(Long codigo) {
        this.codigo = codigo;
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Set<Curso> getCursos() {
        return cursos;
    }

    public void setCursos(Set<Curso> cursos) {
        this.cursos = cursos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Usuario)) return false;

        Usuario usuario = (Usuario) o;

        if (!codigo.equals(usuario.codigo)) return false;
        return nome.equals(usuario.nome);
    }

    @Override
    public int hashCode() {
        int result = codigo.hashCode();
        result = 31 * result + nome.hashCode();
        return result;
    }

}
