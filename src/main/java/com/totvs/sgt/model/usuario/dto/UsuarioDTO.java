package com.totvs.sgt.model.usuario.dto;

import com.totvs.sgt.model.curso.Curso;
import com.totvs.sgt.model.usuario.Usuario;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UsuarioDTO {

    private Long codigo;
    private String nome;
    private Set<Long> cdCursos = new LinkedHashSet();

    public UsuarioDTO(Usuario usuario) {
        this.codigo = usuario.getCodigo();
        this.nome = usuario.getNome();
        if (usuario.getCursos() != null) {
            this.cdCursos = pegaCodigosCursos(usuario.getCursos());
        }
    }

    private Set<Long> pegaCodigosCursos(Set<Curso> cursos) {
        return cursos.stream().map(Curso::getCodigo).collect(Collectors.toSet());
    }

    public Usuario converteParaUsuario() {
        Usuario usuario = new Usuario();
        usuario.setCodigo(this.codigo);
        usuario.setNome(this.nome);
        usuario.setCursos(this.carregaListaCursos(this.cdCursos));

        return usuario;
    }

    private Set<Curso> carregaListaCursos(Set<Long> cdCursos) {
        return cdCursos.stream().map(cdCurso -> {
            return new Curso(cdCurso);
        }).collect(Collectors.toSet());
    }

}
