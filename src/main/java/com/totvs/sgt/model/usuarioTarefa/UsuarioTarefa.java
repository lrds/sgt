package com.totvs.sgt.model.usuarioTarefa;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.totvs.sgt.model.tarefa.Tarefa;
import com.totvs.sgt.model.usuario.Usuario;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "usuarios_tarefas")
public class UsuarioTarefa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long codigo;

    @ManyToOne
    @JoinColumn(name = "codigo_usuario", referencedColumnName = "codigo")
//    @JsonIgnoreProperties({"usuarioTarefas"})
    private Usuario usuario;

    @ManyToOne
    @JoinColumn(name = "codigo_tarefa", referencedColumnName = "codigo")
    @JsonIgnoreProperties({"usuarioTarefas"})
    private Tarefa tarefa;

    private Timestamp dataInicio;

    public UsuarioTarefa(Usuario usuario) {
        this.usuario = usuario;
    }
}
