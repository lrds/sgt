package com.totvs.sgt.repository;

import com.totvs.sgt.model.curso.Curso;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CursoRepository extends JpaRepository<Curso, Long> { }
