package com.totvs.sgt.repository;

import com.totvs.sgt.model.statusTarefa.StatusTarefa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusTarefaRepository extends JpaRepository<StatusTarefa, Long> { }
