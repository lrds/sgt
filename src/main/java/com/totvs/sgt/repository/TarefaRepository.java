package com.totvs.sgt.repository;

import com.totvs.sgt.model.tarefa.Tarefa;
import com.totvs.sgt.model.tarefa.dto.TarefaDTO;
import com.totvs.sgt.model.tarefa.dto.TarefaListaDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface TarefaRepository extends JpaRepository<Tarefa, Long> {

    @Query(" select new com.totvs.sgt.model.tarefa.dto.TarefaDTO(t) " +
           "   from Tarefa t ")
    Page<TarefaDTO> findAllDTO(Pageable pageable);

    @Query("    select new com.totvs.sgt.model.tarefa.dto.TarefaListaDTO(t) " +
           "      from Tarefa t " +
           " left join t.statusTarefa s " +
           "     where (:status is null or s.codigo = :status) " +
           "       and (:pesquisa = '' " +
           "            or lower(t.titulo) like concat('%', lower(:pesquisa), '%') " +
           "            or lower(t.descricao) like concat('%', lower(:pesquisa), '%') ) " )
    Page<TarefaListaDTO> findAllListaDTO(Long status, String pesquisa, Pageable pageable);

    @Query(" select new com.totvs.sgt.model.tarefa.dto.TarefaDTO(t) " +
            "  from Tarefa t " +
            " where t.codigo = :codigo ")
    Optional<TarefaDTO> findByIdDTO(Long codigo);
}
