package com.totvs.sgt.repository;

import com.totvs.sgt.model.usuario.Usuario;
import com.totvs.sgt.model.usuario.dto.UsuarioDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    @Query("    select new com.totvs.sgt.model.usuario.dto.UsuarioDTO(u) " +
           "      from Usuario u " +
           "     where (:pesquisa = '' " +
           "            or lower(u.nome) like concat('%', lower(:pesquisa), '%')) " )
    List<UsuarioDTO> findAllDTO(String pesquisa);

    @Query("    select new com.totvs.sgt.model.usuario.dto.UsuarioDTO(u) " +
           "      from Usuario u " +
           "     where (:pesquisa = '' " +
           "            or lower(u.nome) like concat('%', lower(:pesquisa), '%') ) " )
    Page<UsuarioDTO> findAllPageableDTO(String pesquisa, Pageable pageable);

    @Query(" select new com.totvs.sgt.model.usuario.dto.UsuarioDTO(u) " +
            "  from Usuario u " +
            " where u.codigo = :codigo ")
    Optional<UsuarioDTO> findByIdDTO(Long codigo);

}
