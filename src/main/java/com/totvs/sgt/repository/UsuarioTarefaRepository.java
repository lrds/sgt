package com.totvs.sgt.repository;

import com.totvs.sgt.model.usuarioTarefa.UsuarioTarefa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioTarefaRepository extends JpaRepository<UsuarioTarefa, Long> { }
