package com.totvs.sgt.resource;

import com.totvs.sgt.model.curso.Curso;
import com.totvs.sgt.service.CursoService;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cursos")
public class CursoResource {

    private final CursoService cursoService;

    public CursoResource(CursoService cursoService) {
        this.cursoService = cursoService;
    }

    @GetMapping()
    public ResponseEntity buscarTudo() {
        return ResponseEntity.ok(cursoService.buscarTudo());
    }

    @GetMapping("/paginado/lista")
    public ResponseEntity buscarTudoPaginado(@RequestParam(required = false) String pesquisa,
                                             Pageable pageable) {
        return ResponseEntity.ok(cursoService.buscarTudoPaginado(pesquisa, pageable));
    }

    @GetMapping("/{codigo}")
    public ResponseEntity buscarPorCodigo(@PathVariable Long codigo) {
        return cursoService.buscarPorCodigo(codigo)
                           .map(curso -> ResponseEntity.ok(curso))
                           .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity criar(@RequestBody Curso curso) {
        return ResponseEntity.ok(cursoService.criar(curso));
    }

    @PutMapping
    public ResponseEntity atualizar(@RequestBody Curso curso) {
        return ResponseEntity.ok(cursoService.atualizar(curso));
    }

    @DeleteMapping("/{codigo}")
    public ResponseEntity deletar(@PathVariable Long codigo) {
        return cursoService.deletar(codigo) ? ResponseEntity.ok().build()
                                            : ResponseEntity.notFound().build();
    }

}
