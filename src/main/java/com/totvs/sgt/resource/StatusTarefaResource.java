package com.totvs.sgt.resource;

import com.totvs.sgt.model.statusTarefa.StatusTarefa;
import com.totvs.sgt.service.StatusTarefaService;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/status-tarefas")
public class StatusTarefaResource {

    private final StatusTarefaService statusTarefaService;

    public StatusTarefaResource(StatusTarefaService statusTarefaService) {
        this.statusTarefaService = statusTarefaService;
    }

    @GetMapping
    public ResponseEntity buscarTudo() {
        return ResponseEntity.ok(statusTarefaService.buscarTudo());
    }

    @GetMapping("/paginado")
    public ResponseEntity buscarTudoPaginado(Pageable pageable) {
        return ResponseEntity.ok(statusTarefaService.buscarTudoPaginado(pageable));
    }

    @GetMapping("/{codigo}")
    public ResponseEntity buscarPorCodigo(@PathVariable Long codigo) {
        return statusTarefaService.buscarPorCodigo(codigo)
                                  .map(statusTarefa -> ResponseEntity.ok(statusTarefa))
                                  .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity criar(@RequestBody StatusTarefa statusTarefa) {
        return ResponseEntity.ok(statusTarefaService.criar(statusTarefa));
    }

    @PutMapping
    public ResponseEntity atualizar(@RequestBody StatusTarefa statusTarefa) {
        return ResponseEntity.ok(statusTarefaService.atualizar(statusTarefa));
    }

    @DeleteMapping("/{codigo}")
    public ResponseEntity deletar(@PathVariable Long codigo) {
        return statusTarefaService.deletar(codigo) ? ResponseEntity.ok().build()
                                                  : ResponseEntity.notFound().build();
    }

}
