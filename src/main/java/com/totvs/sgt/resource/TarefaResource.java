package com.totvs.sgt.resource;

import com.totvs.sgt.model.tarefa.dto.TarefaDTO;
import com.totvs.sgt.service.TarefaService;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/tarefas")
public class TarefaResource {

    private final TarefaService tarefaService;

    public TarefaResource(TarefaService tarefaService) {
        this.tarefaService = tarefaService;
    }

    @GetMapping("/paginado")
    public ResponseEntity buscarTudo(Pageable pageable) {
        return ResponseEntity.ok(tarefaService.buscarTudoDTO(pageable));
    }

    @GetMapping("/paginado/lista")
    public ResponseEntity buscarTudoPaginado(@RequestParam(required = false) Long status,
                                             @RequestParam(required = false) String pesquisa,
                                             Pageable pageable) {
        return ResponseEntity.ok(tarefaService.buscarTudoListaDTO(status, pesquisa, pageable));
    }

    @GetMapping("/{codigo}")
    public ResponseEntity buscarPorCodigo(@PathVariable Long codigo) {
        return tarefaService.buscarPorCodigoDTO(codigo)
                            .map(tarefa -> ResponseEntity.ok(tarefa))
                            .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity criar(@RequestBody TarefaDTO tarefaDTO) {
        return ResponseEntity.ok(tarefaService.criar(tarefaDTO));
    }

    @PutMapping
    public ResponseEntity atualizar(@RequestBody TarefaDTO tarefaDTO) {
        return ResponseEntity.ok(tarefaService.atualizar(tarefaDTO));
    }

    @DeleteMapping("/{codigo}")
    public ResponseEntity deletar(@PathVariable Long codigo) {
        return tarefaService.deletar(codigo) ? ResponseEntity.ok().build()
                                             : ResponseEntity.notFound().build();
    }

}
