package com.totvs.sgt.resource;

import com.totvs.sgt.model.usuario.dto.UsuarioDTO;
import com.totvs.sgt.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/usuarios")
public class UsuarioResource {

    @Autowired
    private UsuarioService usuarioService;

    @GetMapping
    public ResponseEntity buscarTudo(@RequestParam(required = false) String pesquisa) {
        return ResponseEntity.ok(usuarioService.buscarTudoDTO(pesquisa));
    }

    @GetMapping("/paginado/lista")
    public ResponseEntity buscarTudoPaginado(@RequestParam(required = false) String pesquisa,
                                             Pageable pageable) {
        return ResponseEntity.ok(usuarioService.buscarTudoPaginadoDTO(pesquisa, pageable));
    }

    @GetMapping("/{codigo}")
    public ResponseEntity buscarPorCodigo(@PathVariable Long codigo) {
        return usuarioService.buscarPorCodigo(codigo)
                                  .map(usuario -> ResponseEntity.ok(usuario))
                                  .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity criar(@RequestBody UsuarioDTO usuarioDTO) {
        return ResponseEntity.ok(usuarioService.criar(usuarioDTO));
    }

    @PutMapping
    public ResponseEntity atualizar(@RequestBody UsuarioDTO usuarioDTO) {
        return ResponseEntity.ok(usuarioService.atualizar(usuarioDTO));
    }

    @DeleteMapping("/{codigo}")
    public ResponseEntity deletar(@PathVariable Long codigo) {
        return usuarioService.deletar(codigo) ? ResponseEntity.ok().build()
                                              : ResponseEntity.notFound().build();
    }

}
