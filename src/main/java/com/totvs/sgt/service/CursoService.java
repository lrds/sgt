package com.totvs.sgt.service;

import com.totvs.sgt.model.curso.Curso;
import com.totvs.sgt.model.dto.RespostaPoUi;
import com.totvs.sgt.repository.CursoRepository;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CursoService {

    private final CursoRepository cursoRepository;

    public CursoService(CursoRepository cursoRepository) {
        this.cursoRepository = cursoRepository;
    }

    public RespostaPoUi buscarTudo() {
        return new RespostaPoUi(cursoRepository.findAll());
    }

    public RespostaPoUi buscarTudoPaginado(String pesquisa, Pageable pageable) {
        if (pesquisa != null && !pesquisa.trim().equals("")) {
            // ERRO só a primeira letra esta ficando minuscula
            pesquisa.toLowerCase();
        } else {
            pesquisa = "";
        }

        ExampleMatcher configuracaoDoFiltro
                = ExampleMatcher.matchingAll()
                                .withIgnoreCase()
                                .withMatcher("descricao", ExampleMatcher.GenericPropertyMatchers.contains());

        Curso curso = new Curso();
        curso.setDescricao(pesquisa);

        Example<Curso> cursoExample = Example.of(curso, configuracaoDoFiltro);

        return new RespostaPoUi(cursoRepository.findAll(cursoExample, pageable));
    }

    public Optional<Curso> buscarPorCodigo(Long codigo) {
        return cursoRepository.findById(codigo);
    }

    public Curso criar(Curso curso) {
        if (curso != null && curso.getCodigo() != null) {
            throw new RuntimeException("Erro ao criar, código do status deve ser nulo");
        }

        return cursoRepository.save(curso);
    }

    public Curso atualizar(Curso curso) {
        validaAtualizar(curso);

        return cursoRepository.save(curso);
    }

    public Boolean deletar(Long codigo) {
        return cursoRepository.findById(codigo)
                                     .map(curso -> {
                                         cursoRepository.delete(curso);
                                         return true;
                                     })
                                     .orElse(false);
    }

    private void validaAtualizar(Curso curso) {
        if (curso == null || curso.getCodigo() == null) {
            throw new RuntimeException("Erro ao atualizar, verifique o código do curso");
        }

        if (!cursoRepository.existsById(curso.getCodigo())) {
            throw new RuntimeException("curso não encontrado");
        }
    }
}
