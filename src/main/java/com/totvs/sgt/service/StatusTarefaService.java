package com.totvs.sgt.service;

import com.totvs.sgt.model.statusTarefa.StatusTarefa;
import com.totvs.sgt.model.dto.RespostaPoUi;
import com.totvs.sgt.repository.StatusTarefaRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class StatusTarefaService {

    private final StatusTarefaRepository statusTarefaRepository;

    public StatusTarefaService(StatusTarefaRepository statusTarefaRepository) {
        this.statusTarefaRepository = statusTarefaRepository;
    }

    public RespostaPoUi buscarTudo() {
        return new RespostaPoUi(statusTarefaRepository.findAll());
    }

    public RespostaPoUi buscarTudoPaginado(Pageable pageable) {
        return new RespostaPoUi(statusTarefaRepository.findAll(pageable));
    }

    public Optional<StatusTarefa> buscarPorCodigo(Long codigo) {
        return statusTarefaRepository.findById(codigo);
    }

    public StatusTarefa criar(StatusTarefa statusTarefa) {
        if (statusTarefa != null && statusTarefa.getCodigo() != null) {
            throw new RuntimeException("Erro ao criar, código do status deve ser nulo");
        }

        return statusTarefaRepository.save(statusTarefa);
    }

    public StatusTarefa atualizar(StatusTarefa statusTarefa) {
        validaAtualizar(statusTarefa);

        return statusTarefaRepository.save(statusTarefa);
    }

    public Boolean deletar(Long codigo) {
        return statusTarefaRepository.findById(codigo)
                                     .map(statusTarefa -> {
                                         statusTarefaRepository.delete(statusTarefa);
                                         return true;
                                     })
                                     .orElse(false);
    }

    private void validaAtualizar(StatusTarefa statusTarefa) {
        if (statusTarefa == null || statusTarefa.getCodigo() == null) {
            throw new RuntimeException("Erro ao atualizar, verifique o código do status");
        }

        if (!statusTarefaRepository.existsById(statusTarefa.getCodigo())) {
            throw new RuntimeException("Status não encontrado");
        }
    }
}
