package com.totvs.sgt.service;

import com.totvs.sgt.model.tarefa.Tarefa;
import com.totvs.sgt.model.dto.RespostaPoUi;
import com.totvs.sgt.model.tarefa.dto.TarefaDTO;
import com.totvs.sgt.repository.TarefaRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TarefaService {

    private final TarefaRepository tarefaRepository;

    public TarefaService(TarefaRepository tarefaRepository) {
        this.tarefaRepository = tarefaRepository;
    }

    public RespostaPoUi buscarTudoDTO(Pageable pageable) {
        return new RespostaPoUi(tarefaRepository.findAllDTO(pageable));
    }

    public RespostaPoUi buscarTudoListaDTO(Long status, String pesquisa, Pageable pageable) {
        if (status < 1) {
            status = null;
        }

        if (pesquisa != null && !pesquisa.trim().equals("")) {
            // ERRO só a primeira letra esta ficando minuscula
            pesquisa.toLowerCase();
        } else {
            pesquisa = "";
        }

        return new RespostaPoUi(tarefaRepository.findAllListaDTO(status, pesquisa, pageable));
    }

    public Optional<TarefaDTO> buscarPorCodigoDTO(Long codigo) {
        return tarefaRepository.findByIdDTO(codigo);
    }

    public Tarefa criar(TarefaDTO tarefaDTO) {
        Tarefa tarefa = tarefaDTO.converteParaTarefa();

        if (tarefa != null && tarefa.getCodigo() != null) {
            throw new RuntimeException("Erro ao criar, código do status deve ser nulo");
        }

        tarefa.setaUsuarios();

        return tarefaRepository.save(tarefa);
    }

    public Tarefa atualizar(TarefaDTO tarefaDTO) {
        Tarefa tarefa = tarefaDTO.converteParaTarefa();

        validaAtualizar(tarefa);

        tarefa.setaUsuarios();

        return tarefaRepository.save(tarefa);
    }

    public Boolean deletar(Long codigo) {
        return tarefaRepository.findById(codigo)
                                     .map(tarefa -> {
                                         tarefaRepository.delete(tarefa);
                                         return true;
                                     })
                                     .orElse(false);
    }

    private void validaAtualizar(Tarefa tarefa) {
        if (tarefa == null || tarefa.getCodigo() == null) {
            throw new RuntimeException("Erro ao atualizar, verifique o código do tarefa");
        }

        if (!tarefaRepository.existsById(tarefa.getCodigo())) {
            throw new RuntimeException("Tarefa não encontrado");
        }
    }
}
