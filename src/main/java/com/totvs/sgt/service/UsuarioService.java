package com.totvs.sgt.service;

import com.totvs.sgt.model.dto.RespostaPoUi;
import com.totvs.sgt.model.usuario.Usuario;
import com.totvs.sgt.model.usuario.dto.UsuarioDTO;
import com.totvs.sgt.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public RespostaPoUi buscarTudoDTO(@PathVariable(required = false) String pesquisa) {
        if (pesquisa != null && !pesquisa.trim().equals("")) {
            // ERRO só a primeira letra esta ficando minuscula
            pesquisa.toLowerCase();
        } else {
            pesquisa = "";
        }

        return new RespostaPoUi(usuarioRepository.findAllDTO(pesquisa));
    }

    public RespostaPoUi buscarTudoPaginadoDTO(String pesquisa, Pageable pageable) {
        if (pesquisa != null && !pesquisa.trim().equals("")) {
            // ERRO só a primeira letra esta ficando minuscula
            pesquisa.toLowerCase();
        } else {
            pesquisa = "";
        }

        return new RespostaPoUi(usuarioRepository.findAllPageableDTO(pesquisa, pageable));
    }

    public Optional<UsuarioDTO> buscarPorCodigo(Long codigo) {
        return usuarioRepository.findByIdDTO(codigo);
    }

    public Usuario criar(UsuarioDTO usuarioDTO) {

        Usuario usuario = usuarioDTO.converteParaUsuario();

        if (usuario != null && usuario.getCodigo() != null) {
            throw new RuntimeException("Erro ao criar, código do status deve ser nulo");
        }

        return usuarioRepository.save(usuario);
    }

    public Usuario atualizar(UsuarioDTO usuarioDTO) {
        Usuario usuario = usuarioDTO.converteParaUsuario();

        validaAtualizar(usuario);

        return usuarioRepository.save(usuario);
    }

    public Boolean deletar(Long codigo) {
        return usuarioRepository.findById(codigo)
                                     .map(usuario -> {
                                         usuarioRepository.delete(usuario);
                                         return true;
                                     })
                                     .orElse(false);
    }

    private void validaAtualizar(Usuario usuario) {
        if (usuario == null || usuario.getCodigo() == null) {
            throw new RuntimeException("Erro ao atualizar, verifique o código do usuário");
        }

        if (!usuarioRepository.existsById(usuario.getCodigo())) {
            throw new RuntimeException("Usuário não encontrado");
        }
    }
}
