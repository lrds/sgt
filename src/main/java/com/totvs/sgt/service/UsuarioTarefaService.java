package com.totvs.sgt.service;

import com.totvs.sgt.model.usuarioTarefa.UsuarioTarefa;
import com.totvs.sgt.repository.UsuarioTarefaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UsuarioTarefaService {

    private final UsuarioTarefaRepository usuarioTarefaRepository;

    public UsuarioTarefaService(UsuarioTarefaRepository usuarioTarefaRepository) {
        this.usuarioTarefaRepository = usuarioTarefaRepository;
    }

    public List<UsuarioTarefa> buscarTudo() {
        return usuarioTarefaRepository.findAll();
    }

    public Page<UsuarioTarefa> buscarTudoPaginado(Pageable pageable) {
        return usuarioTarefaRepository.findAll(pageable);
    }

    public Optional<UsuarioTarefa> buscarPorCodigo(Long codigo) {
        return usuarioTarefaRepository.findById(codigo);
    }

    public UsuarioTarefa criar(UsuarioTarefa usuarioTarefa) {
        if (usuarioTarefa != null && usuarioTarefa.getCodigo() != null) {
            throw new RuntimeException("Erro ao criar, código do status deve ser nulo");
        }

        return usuarioTarefaRepository.save(usuarioTarefa);
    }

    public UsuarioTarefa atualizar(UsuarioTarefa usuarioTarefa) {
        validaAtualizar(usuarioTarefa);

        return usuarioTarefaRepository.save(usuarioTarefa);
    }

    public Boolean deletar(Long codigo) {
        return usuarioTarefaRepository.findById(codigo)
                                     .map(usuarioTarefa -> {
                                         usuarioTarefaRepository.delete(usuarioTarefa);
                                         return true;
                                     })
                                     .orElse(false);
    }

    private void validaAtualizar(UsuarioTarefa usuarioTarefa) {
        if (usuarioTarefa == null || usuarioTarefa.getCodigo() == null) {
            throw new RuntimeException("Erro ao atualizar, verifique o código do Usuário e Tarefa");
        }

        if (!usuarioTarefaRepository.existsById(usuarioTarefa.getCodigo())) {
            throw new RuntimeException("Usuário ou tarefa não encontrado");
        }
    }
}
